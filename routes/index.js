var express = require('express');
var router = express.Router();
var dbService = require('../middleware/db_connection');
const productControllers = require('../controllers/addProductController');
const formFiledController = require('../controllers/sendFormFiledsController');
router.get('/getTvFormFields', formFiledController.sendTvFormFields);
router.post('/addCategory', dbService.databaseConnection, productControllers.addProduct);
router.get('/get_products', dbService.databaseConnection, productControllers.getProducts);
router.get('/get_products/:id', dbService.databaseConnection, productControllers.getProductsById);
router.put('/addCategory', dbService.databaseConnection, productControllers.upDtateProduct);
module.exports = router;
