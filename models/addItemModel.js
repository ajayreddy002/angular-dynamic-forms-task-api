const mySql = require('mysql');
module.exports = {
    addItemTodb: (params, conn) => {
        return new Promise((resolve, reject) => {
            const insertQuery = mySql.format('INSERT INTO products (name, description, height, width, is_smart) VALUES (?,?,?,?,?)', [params.name, params.description, params.height, params.width, params.is_smart]);
            conn.query(insertQuery, (err, data) => {
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            });
        });
    },
    getItems: (conn) => {
        return new Promise((resolve, reject) => {
            const select_query = mySql.format('SELECT * FROM products');
            conn.query(select_query, (err, data) => {
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            });
        });
    },
    getItemById: (param, conn) => {
        console.log(param.id)
        return new Promise((resolve, reject) => {
            const select_query = mySql.format('SELECT * FROM products WHERE id = ?', [param.id]);
            conn.query(select_query, (err, data) => {
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            });
        });
    },
    updateItem: (param, conn) => {
        console.log(param.id)
        return new Promise((resolve, reject) => {
            const update_query = mySql.format('UPDATE products SET name = ?, description = ?, height = ?, width = ?, is_smart = ? WHERE id = ?',
                [param.name, param.description, param.height, param.width, param.is_smart, param.id]);
            conn.query(update_query, (err, data) => {
                if (data) {
                    resolve(data);
                } else {
                    reject(err);
                }
            });
        });
    }
}