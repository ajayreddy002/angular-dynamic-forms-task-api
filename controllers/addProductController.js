const addItemModel = require('../models/addItemModel')
module.exports = {
    addProduct: async (req, res) => {
        if (req.body.name && req.body.description) {
            const insertData = await addItemModel.addItemTodb(req.body, req.mysqlconn);
            if (insertData && insertData.insertId) {
                res.send({
                    message: 'Added successfully',
                    status_code: 100,
                    error: true
                });
            }
        } else {
            res.send({
                message: 'Required parameters are missing',
                status_code: 160
            });
        }
    },
    getProducts: async (req, res) => {
        const listProducts = await addItemModel.getItems(req.mysqlconn);
        if (listProducts) {
            res.send({
                message: 'Added successfully',
                status_code: 100,
                result: listProducts,
                error: false
            });
        } else {
            res.send({
                message: 'Something went wrong',
                status_code: 160
            });
        }
    },
    getProductsById: async (req, res) => {
        if (req.params.id) {
            const itemData = await addItemModel.getItemById(req.params, req.mysqlconn);
            if (itemData) {
                res.send({
                    message: 'Added successfully',
                    status_code: 100,
                    result: itemData
                });
            } else {
                res.send({
                    message: 'Something went wrong',
                    status_code: 160,
                    error: true
                });
            }
        } else {
            res.send({
                message: 'Required parameters are missing',
                status_code: 106,
                error: true
            });
        }
    },
    upDtateProduct: async (req, res) => {
        if (req.body.id) {
            const updatedData = await addItemModel.updateItem(req.body, req.mysqlconn);
            if (updatedData && updatedData.affectedRows) {
                res.send({
                    message: 'Updated successfully',
                    status_code: 100,
                    result: [updatedData.affectedRows]
                });
            } else {
                res.send({
                    message: 'Something went wrong',
                    status_code: 160
                });
            }
        } else {
            res.send({
                message: 'Required parameters are missing',
                status_code: 106,
                error: true
            });
        }
    }
}