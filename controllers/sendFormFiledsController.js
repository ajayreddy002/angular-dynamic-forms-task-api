module.exports = {
    sendTvFormFields: (req, res) => {
        const formFields = [
            {
              caption: 'Name',
              type: 'text',
              mandatory: true,
              defaultValue: '',
              validationMessage: 'Name is mandatory',
              name: 'name'
            },
            {
              caption: 'Description',
              type: 'text',
              mandatory: true,
              defaultValue: 'Please enter something here',
              validationMessage: 'Oops you can\'t have empty',
              name: 'description'
            },
            {
              caption: 'Height',
              type: 'number',
              mandatory: false,
              defaultValue: '',
              validationMessage: '',
              name: 'height'
            },
            {
              caption: 'Width',
              type: 'number',
              mandatory: false,
              defaultValue: '',
              validationMessage: '',
              name: 'width'
            },
            {
              caption: 'IsSmart',
              type: 'checkbox',
              mandatory: false,
              defaultValue: 'false',
              validationMessage: '',
              name: 'is_smart'
            },
            {
                caption: 'id',
                type: 'text',
                mandatory: false,
                defaultValue: '',
                validationMessage: '',
                name: 'id'
              }
          ];
          res.send(formFields);
    }
}