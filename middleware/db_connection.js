var env = require('dotenv').config();
var mysql = require('mysql');
let pool;
const DBConfig = {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
};
module.exports = {
    databaseConnection: (req, res, next) => {
        if(pool) {
            passConnection(req, res, next);
        } else {
            pool = mysql.createPool(DBConfig);
            passConnection(req, res, next);
        }
    }
}
    passConnection = (req, res, next) => {
        pool.getConnection((err, connection) => {
            if (err) {
                res.send(err);
                console.log(err);
            } else {
                req.mysqlconn = connection;
                console.log('Db Connetcted Successfully');
            }
            next();
            res.on('finish', () => {
                connection.release();
            })
        });
    }
